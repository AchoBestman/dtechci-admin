<?php
use Illuminate\Support\Facades\DB;
use App\Models\Exchange;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

function validationFormatErrors($validator){

  $errors = $validator->getMessageBag()->toArray();
  $formattedErrors = [];

  foreach ($errors as $field => $messages) {
    foreach ($messages as $message) {
      $formattedErrors[$field] = $message;
      break;
    }
  }

  return $formattedErrors;
}


function image_url($url){

  $path = explode('uploads', $url);

  if(!isset($path) || (isset($path) && !isset($path[1]))){
    return "";
  }

  $base_url = env("APP_URL");
  if($base_url === "http://localhost:8000"){
    $base_url = $base_url.'/uploads';
  }
  else{
    $base_url = $base_url.'/public/uploads';
  }

  return $base_url.$path[1];
}


function upload(Request $request){

  try {
      if (isset($request['file'])) {
          $file = $request->file;
          $fileName = $file->getClientOriginalName();
          $file->move(public_path('uploads'), $fileName);
          return "uploads/$fileName";
      }
  } catch (\Exception $e) {
    return response(['message'=>$e->getMessage()], 500);
  }

}
