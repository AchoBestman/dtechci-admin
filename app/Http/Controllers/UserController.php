<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Illuminate\Support\Facades\Hash;

class UserController extends AppBaseController
{
    /** @var UserRepository $userRepository*/
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->userRepository->all();

        $filteredUsers = $users->filter(function ($user) {
            return $user->role !== 'root';
        });

        return view('users.index')
            ->with('users', $filteredUsers);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $avatar = "";
        if(isset($input['avatar'])){
            $request['file']=$input['avatar'];
            $input['avatar'] = upload($request);
        }
        $input['role'] = "user";
        $input['password'] = Hash::make($input['password']);
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $user = $this->userRepository->create($input);

        Flash::success('User saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $input = $request->except('password');
        $avatar = "";
        if(isset($input['avatar'])){
            $request['file']=$input['avatar'];
            $input['avatar'] = upload($request);
        }

        if(isset($request['password']) && $request['password'] != null && strlen($request['password'])>5){
            $input['password'] = Hash::make($request['password']);
        }
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $user = $this->userRepository->update($input, $id);

        Flash::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        if($user->email == "root@gmail.com"){
            Flash::error('You can not delete the root user.');
        }
        if($user->id == auth()->user()->id){
            Flash::error('You can not delete this account. It is the current account connected.');
        }
        else{
            $this->userRepository->delete($id);
            Flash::success('User deleted successfully.');
        }

        return redirect(route('users.index'));
    }
}
