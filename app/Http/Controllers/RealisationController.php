<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRealisationRequest;
use App\Http\Requests\UpdateRealisationRequest;
use App\Repositories\RealisationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class RealisationController extends AppBaseController
{
    /** @var RealisationRepository $realisationRepository*/
    private $realisationRepository;

    public function __construct(RealisationRepository $realisationRepo)
    {
        $this->realisationRepository = $realisationRepo;
    }

    /**
     * Display a listing of the Realisation.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $realisations = $this->realisationRepository->all([], null, null, ['*'], ['ordre', 'asc']);

        return view('realisations.index')
            ->with('realisations', $realisations);
    }

    /**
     * Show the form for creating a new Realisation.
     *
     * @return Response
     */
    public function create()
    {
        return view('realisations.create');
    }

    /**
     * Store a newly created Realisation in storage.
     *
     * @param CreateRealisationRequest $request
     *
     * @return Response
     */
    public function store(CreateRealisationRequest $request)
    {
        $input = $request->all();
        $image = "";
        if(isset($input['image'])){
            $request['file']=$input['image'];
            $input['image'] = upload($request);
        }
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $input['user_id'] = auth()->user()->id;

        $realisation = $this->realisationRepository->create($input);

        Flash::success('Realisation saved successfully.');

        return redirect(route('realisations.index'));
    }

    /**
     * Display the specified Realisation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $realisation = $this->realisationRepository->find($id);

        if (empty($realisation)) {
            Flash::error('Realisation not found');

            return redirect(route('realisations.index'));
        }

        return view('realisations.show')->with('realisation', $realisation);
    }

    /**
     * Show the form for editing the specified Realisation.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $realisation = $this->realisationRepository->find($id);

        if (empty($realisation)) {
            Flash::error('Realisation not found');

            return redirect(route('realisations.index'));
        }

        return view('realisations.edit')->with('realisation', $realisation);
    }

    /**
     * Update the specified Realisation in storage.
     *
     * @param int $id
     * @param UpdateRealisationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRealisationRequest $request)
    {
        $realisation = $this->realisationRepository->find($id);

        if (empty($realisation)) {
            Flash::error('Realisation not found');

            return redirect(route('realisations.index'));
        }

        $input = $request->all();
        $image = "";
        if(isset($input['image'])){
            $request['file']=$input['image'];
            $input['image'] = upload($request);
        }
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $input['user_id'] = auth()->user()->id;

        $realisation = $this->realisationRepository->update($input, $id);

        Flash::success('Realisation updated successfully.');

        return redirect(route('realisations.index'));
    }

    /**
     * Remove the specified Realisation from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $realisation = $this->realisationRepository->find($id);

        if (empty($realisation)) {
            Flash::error('Realisation not found');

            return redirect(route('realisations.index'));
        }

        $this->realisationRepository->delete($id);

        Flash::success('Realisation deleted successfully.');

        return redirect(route('realisations.index'));
    }
}
