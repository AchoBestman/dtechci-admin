<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSubProjectAPIRequest;
use App\Http\Requests\API\UpdateSubProjectAPIRequest;
use App\Models\SubProject;
use App\Repositories\SubProjectRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SubProjectController
 * @package App\Http\Controllers\API
 */

class SubProjectAPIController extends AppBaseController
{
    /** @var  SubProjectRepository */
    private $subProjectRepository;

    public function __construct(SubProjectRepository $subProjectRepo)
    {
        $this->subProjectRepository = $subProjectRepo;
    }

    /**
     * Display a listing of the SubProject.
     * GET|HEAD /subProjects
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $subProjects = $this->subProjectRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit'),
            ['*'], 
            ['ordre', 'asc']
        );

        return $this->sendResponse($subProjects->toArray(), 'Sub Projects retrieved successfully');
    }

    /**
     * Store a newly created SubProject in storage.
     * POST /subProjects
     *
     * @param CreateSubProjectAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSubProjectAPIRequest $request)
    {
        $input = $request->all();

        $subProject = $this->subProjectRepository->create($input);

        return $this->sendResponse($subProject->toArray(), 'Sub Project saved successfully');
    }

    /**
     * Display the specified SubProject.
     * GET|HEAD /subProjects/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SubProject $subProject */
        $subProject = $this->subProjectRepository->find($id);

        if (empty($subProject)) {
            return $this->sendError('Sub Project not found');
        }

        return $this->sendResponse($subProject->toArray(), 'Sub Project retrieved successfully');
    }

    /**
     * Update the specified SubProject in storage.
     * PUT/PATCH /subProjects/{id}
     *
     * @param int $id
     * @param UpdateSubProjectAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubProjectAPIRequest $request)
    {
        $input = $request->all();

        /** @var SubProject $subProject */
        $subProject = $this->subProjectRepository->find($id);

        if (empty($subProject)) {
            return $this->sendError('Sub Project not found');
        }

        $subProject = $this->subProjectRepository->update($input, $id);

        return $this->sendResponse($subProject->toArray(), 'SubProject updated successfully');
    }

    /**
     * Remove the specified SubProject from storage.
     * DELETE /subProjects/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SubProject $subProject */
        $subProject = $this->subProjectRepository->find($id);

        if (empty($subProject)) {
            return $this->sendError('Sub Project not found');
        }

        $subProject->delete();

        return $this->sendSuccess('Sub Project deleted successfully');
    }
}
