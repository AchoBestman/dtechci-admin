<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSlideItemAPIRequest;
use App\Http\Requests\API\UpdateSlideItemAPIRequest;
use App\Models\SlideItem;
use App\Repositories\SlideItemRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SlideItemController
 * @package App\Http\Controllers\API
 */

class SlideItemAPIController extends AppBaseController
{
    /** @var  SlideItemRepository */
    private $slideItemRepository;

    public function __construct(SlideItemRepository $slideItemRepo)
    {
        $this->slideItemRepository = $slideItemRepo;
    }

    /**
     * Display a listing of the SlideItem.
     * GET|HEAD /slideItems
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $slideItems = $this->slideItemRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit'),
            ['*'], 
            ['ordre', 'asc']
        );

        return $this->sendResponse($slideItems->toArray(), 'Slide Items retrieved successfully');
    }

    /**
     * Store a newly created SlideItem in storage.
     * POST /slideItems
     *
     * @param CreateSlideItemAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSlideItemAPIRequest $request)
    {
        $input = $request->all();

        $slideItem = $this->slideItemRepository->create($input);

        return $this->sendResponse($slideItem->toArray(), 'Slide Item saved successfully');
    }

    /**
     * Display the specified SlideItem.
     * GET|HEAD /slideItems/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SlideItem $slideItem */
        $slideItem = $this->slideItemRepository->find($id);

        if (empty($slideItem)) {
            return $this->sendError('Slide Item not found');
        }

        return $this->sendResponse($slideItem->toArray(), 'Slide Item retrieved successfully');
    }

    /**
     * Update the specified SlideItem in storage.
     * PUT/PATCH /slideItems/{id}
     *
     * @param int $id
     * @param UpdateSlideItemAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSlideItemAPIRequest $request)
    {
        $input = $request->all();

        /** @var SlideItem $slideItem */
        $slideItem = $this->slideItemRepository->find($id);

        if (empty($slideItem)) {
            return $this->sendError('Slide Item not found');
        }

        $slideItem = $this->slideItemRepository->update($input, $id);

        return $this->sendResponse($slideItem->toArray(), 'SlideItem updated successfully');
    }

    /**
     * Remove the specified SlideItem from storage.
     * DELETE /slideItems/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SlideItem $slideItem */
        $slideItem = $this->slideItemRepository->find($id);

        if (empty($slideItem)) {
            return $this->sendError('Slide Item not found');
        }

        $slideItem->delete();

        return $this->sendSuccess('Slide Item deleted successfully');
    }
}
