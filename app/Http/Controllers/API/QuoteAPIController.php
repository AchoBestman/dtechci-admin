<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateQuoteAPIRequest;
use App\Http\Requests\API\UpdateQuoteAPIRequest;
use App\Models\Quote;
use App\Repositories\QuoteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class QuoteController
 * @package App\Http\Controllers\API
 */

class QuoteAPIController extends AppBaseController
{
    /** @var  QuoteRepository */
    private $quoteRepository;

    public function __construct(QuoteRepository $quoteRepo)
    {
        $this->quoteRepository = $quoteRepo;
    }

    /**
     * Display a listing of the Quote.
     * GET|HEAD /quotes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $quotes = $this->quoteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($quotes->toArray(), 'Quotes retrieved successfully');
    }

    /**
     * Store a newly created Quote in storage.
     * POST /quotes
     *
     * @param CreateQuoteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateQuoteAPIRequest $request)
    {
        $input = $request->all();

        $quote = $this->quoteRepository->create($input);

        return $this->sendResponse($quote->toArray(), 'Quote saved successfully');
    }

    /**
     * Display the specified Quote.
     * GET|HEAD /quotes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Quote $quote */
        $quote = $this->quoteRepository->find($id);

        if (empty($quote)) {
            return $this->sendError('Quote not found');
        }

        return $this->sendResponse($quote->toArray(), 'Quote retrieved successfully');
    }

    /**
     * Update the specified Quote in storage.
     * PUT/PATCH /quotes/{id}
     *
     * @param int $id
     * @param UpdateQuoteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateQuoteAPIRequest $request)
    {
        $input = $request->all();

        /** @var Quote $quote */
        $quote = $this->quoteRepository->find($id);

        if (empty($quote)) {
            return $this->sendError('Quote not found');
        }

        $quote = $this->quoteRepository->update($input, $id);

        return $this->sendResponse($quote->toArray(), 'Quote updated successfully');
    }

    /**
     * Remove the specified Quote from storage.
     * DELETE /quotes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Quote $quote */
        $quote = $this->quoteRepository->find($id);

        if (empty($quote)) {
            return $this->sendError('Quote not found');
        }

        $quote->delete();

        return $this->sendSuccess('Quote deleted successfully');
    }
}
