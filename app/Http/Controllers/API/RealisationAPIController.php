<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRealisationAPIRequest;
use App\Http\Requests\API\UpdateRealisationAPIRequest;
use App\Models\Realisation;
use App\Repositories\RealisationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class RealisationController
 * @package App\Http\Controllers\API
 */

class RealisationAPIController extends AppBaseController
{
    /** @var  RealisationRepository */
    private $realisationRepository;

    public function __construct(RealisationRepository $realisationRepo)
    {
        $this->realisationRepository = $realisationRepo;
    }

    /**
     * Display a listing of the Realisation.
     * GET|HEAD /realisations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $realisations = $this->realisationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit'),
            ['*'], 
            ['ordre', 'asc']
        );

        return $this->sendResponse($realisations->toArray(), 'Realisations retrieved successfully');
    }
    /**
     * Store a newly created Realisation in storage.
     * POST /realisations
     *
     * @param CreateRealisationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRealisationAPIRequest $request)
    {
        $input = $request->all();

        $realisation = $this->realisationRepository->create($input);

        return $this->sendResponse($realisation->toArray(), 'Realisation saved successfully');
    }

    /**
     * Display the specified Realisation.
     * GET|HEAD /realisations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Realisation $realisation */
        $realisation = $this->realisationRepository->find($id);

        if (empty($realisation)) {
            return $this->sendError('Realisation not found');
        }

        return $this->sendResponse($realisation->toArray(), 'Realisation retrieved successfully');
    }

    /**
     * Update the specified Realisation in storage.
     * PUT/PATCH /realisations/{id}
     *
     * @param int $id
     * @param UpdateRealisationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRealisationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Realisation $realisation */
        $realisation = $this->realisationRepository->find($id);

        if (empty($realisation)) {
            return $this->sendError('Realisation not found');
        }

        $realisation = $this->realisationRepository->update($input, $id);

        return $this->sendResponse($realisation->toArray(), 'Realisation updated successfully');
    }

    /**
     * Remove the specified Realisation from storage.
     * DELETE /realisations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Realisation $realisation */
        $realisation = $this->realisationRepository->find($id);

        if (empty($realisation)) {
            return $this->sendError('Realisation not found');
        }

        $realisation->delete();

        return $this->sendSuccess('Realisation deleted successfully');
    }
}
