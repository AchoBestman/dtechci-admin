<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAppAPIRequest;
use App\Http\Requests\API\UpdateAppAPIRequest;
use App\Models\App;
use App\Repositories\AppRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AppController
 * @package App\Http\Controllers\API
 */

class AppAPIController extends AppBaseController
{
    /** @var  AppRepository */
    private $appRepository;

    public function __construct(AppRepository $appRepo)
    {
        $this->appRepository = $appRepo;
    }

    /**
     * Display a listing of the App.
     * GET|HEAD /apps
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $apps = $this->appRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit'),
            ['*'], 
            ['ordre', 'asc']
        );

        return $this->sendResponse($apps->toArray(), 'Apps retrieved successfully');
    }

    /**
     * Store a newly created App in storage.
     * POST /apps
     *
     * @param CreateAppAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAppAPIRequest $request)
    {
        $input = $request->all();

        $app = $this->appRepository->create($input);

        return $this->sendResponse($app->toArray(), 'App saved successfully');
    }

    /**
     * Display the specified App.
     * GET|HEAD /apps/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var App $app */
        $app = $this->appRepository->find($id);

        if (empty($app)) {
            return $this->sendError('App not found');
        }

        return $this->sendResponse($app->toArray(), 'App retrieved successfully');
    }

    /**
     * Update the specified App in storage.
     * PUT/PATCH /apps/{id}
     *
     * @param int $id
     * @param UpdateAppAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAppAPIRequest $request)
    {
        $input = $request->all();

        /** @var App $app */
        $app = $this->appRepository->find($id);

        if (empty($app)) {
            return $this->sendError('App not found');
        }

        $app = $this->appRepository->update($input, $id);

        return $this->sendResponse($app->toArray(), 'App updated successfully');
    }

    /**
     * Remove the specified App from storage.
     * DELETE /apps/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var App $app */
        $app = $this->appRepository->find($id);

        if (empty($app)) {
            return $this->sendError('App not found');
        }

        $app->delete();

        return $this->sendSuccess('App deleted successfully');
    }
}
