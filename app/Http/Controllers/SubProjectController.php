<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubProjectRequest;
use App\Http\Requests\UpdateSubProjectRequest;
use App\Repositories\SubProjectRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SubProjectController extends AppBaseController
{
    /** @var SubProjectRepository $subProjectRepository*/
    private $subProjectRepository;

    public function __construct(SubProjectRepository $subProjectRepo)
    {
        $this->subProjectRepository = $subProjectRepo;
    }

    /**
     * Display a listing of the SubProject.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $subProjects = $this->subProjectRepository->all([], null, null, ['*'], ['ordre', 'asc']);

        return view('sub_projects.index')
            ->with('subProjects', $subProjects);
    }

    /**
     * Show the form for creating a new SubProject.
     *
     * @return Response
     */
    public function create()
    {
        return view('sub_projects.create');
    }

    /**
     * Store a newly created SubProject in storage.
     *
     * @param CreateSubProjectRequest $request
     *
     * @return Response
     */
    public function store(CreateSubProjectRequest $request)
    {
        $input = $request->all();
        $image = "";
        if(isset($input['image'])){
            $request['file']=$input['image'];
            $input['image'] = upload($request);
        }
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $input['user_id'] = auth()->user()->id;
        $subProject = $this->subProjectRepository->create($input);

        Flash::success('Sub Project saved successfully.');

        return redirect(route('subProjects.index'));
    }

    /**
     * Display the specified SubProject.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subProject = $this->subProjectRepository->find($id);

        if (empty($subProject)) {
            Flash::error('Sub Project not found');

            return redirect(route('subProjects.index'));
        }

        return view('sub_projects.show')->with('subProject', $subProject);
    }

    /**
     * Show the form for editing the specified SubProject.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subProject = $this->subProjectRepository->find($id);

        if (empty($subProject)) {
            Flash::error('Sub Project not found');

            return redirect(route('subProjects.index'));
        }

        return view('sub_projects.edit')->with('subProject', $subProject);
    }

    /**
     * Update the specified SubProject in storage.
     *
     * @param int $id
     * @param UpdateSubProjectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubProjectRequest $request)
    {
        $subProject = $this->subProjectRepository->find($id);

        if (empty($subProject)) {
            Flash::error('Sub Project not found');

            return redirect(route('subProjects.index'));
        }

        $input = $request->all();
        $image = "";
        if(isset($input['image'])){
            $request['file']=$input['image'];
            $input['image'] = upload($request);
        }
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $input['user_id'] = auth()->user()->id;

        $subProject = $this->subProjectRepository->update($input, $id);

        Flash::success('Sub Project updated successfully.');

        return redirect(route('subProjects.index'));
    }

    /**
     * Remove the specified SubProject from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subProject = $this->subProjectRepository->find($id);

        if (empty($subProject)) {
            Flash::error('Sub Project not found');

            return redirect(route('subProjects.index'));
        }

        $this->subProjectRepository->delete($id);

        Flash::success('Sub Project deleted successfully.');

        return redirect(route('subProjects.index'));
    }
}
