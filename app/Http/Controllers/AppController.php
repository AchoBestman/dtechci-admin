<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAppRequest;
use App\Http\Requests\UpdateAppRequest;
use App\Repositories\AppRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class AppController extends AppBaseController
{
    /** @var AppRepository $appRepository*/
    private $appRepository;

    public function __construct(AppRepository $appRepo)
    {
        $this->appRepository = $appRepo;
    }

    /**
     * Display a listing of the App.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $apps = $this->appRepository->all([], null, null, ['*'], ['ordre', 'asc']);

        return view('apps.index')
            ->with('apps', $apps);
    } 

    /**
     * Show the form for creating a new App.
     *
     * @return Response
     */
    public function create()
    {
        return view('apps.create');
    }

    /**
     * Store a newly created App in storage.
     *
     * @param CreateAppRequest $request
     *
     * @return Response
     */
    public function store(CreateAppRequest $request)
    {
        $input = $request->all();
        $image = "";
        if(isset($input['image'])){
            $request['file']=$input['image'];
            $input['image'] = upload($request);
        }
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $input['user_id'] = auth()->user()->id;
        $app = $this->appRepository->create($input);

        Flash::success('App saved successfully.');

        return redirect(route('apps.index'));
    }

    /**
     * Display the specified App.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $app = $this->appRepository->find($id);

        if (empty($app)) {
            Flash::error('App not found');

            return redirect(route('apps.index'));
        }

        return view('apps.show')->with('app', $app);
    }

    /**
     * Show the form for editing the specified App.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $app = $this->appRepository->find($id);

        if (empty($app)) {
            Flash::error('App not found');

            return redirect(route('apps.index'));
        }

        return view('apps.edit')->with('app', $app);
    }

    /**
     * Update the specified App in storage.
     *
     * @param int $id
     * @param UpdateAppRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAppRequest $request)
    {
        $app = $this->appRepository->find($id);

        if (empty($app)) {
            Flash::error('App not found');

            return redirect(route('apps.index'));
        }

        $input = $request->all();
        $image = "";
        if(isset($input['image'])){
            $request['file']=$input['image'];
            $input['image'] = upload($request);
        }
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $input['user_id'] = auth()->user()->id;

        $app = $this->appRepository->update($input, $id);

        Flash::success('App updated successfully.');

        return redirect(route('apps.index'));
    }

    /**
     * Remove the specified App from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $app = $this->appRepository->find($id);

        if (empty($app)) {
            Flash::error('App not found');

            return redirect(route('apps.index'));
        }

        $this->appRepository->delete($id);

        Flash::success('App deleted successfully.');

        return redirect(route('apps.index'));
    }
}
