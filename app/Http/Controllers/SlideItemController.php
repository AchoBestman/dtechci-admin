<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSlideItemRequest;
use App\Http\Requests\UpdateSlideItemRequest;
use App\Repositories\SlideItemRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SlideItemController extends AppBaseController
{
    /** @var SlideItemRepository $slideItemRepository*/
    private $slideItemRepository;

    public function __construct(SlideItemRepository $slideItemRepo)
    {
        $this->slideItemRepository = $slideItemRepo;
    }

    /**
     * Display a listing of the SlideItem.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $slideItems = $this->slideItemRepository->all([], null, null, ['*'], ['ordre', 'asc']);

        return view('slide_items.index')
            ->with('slideItems', $slideItems);
    }

    /**
     * Show the form for creating a new SlideItem.
     *
     * @return Response
     */
    public function create()
    {
        return view('slide_items.create');
    }

    /**
     * Store a newly created SlideItem in storage.
     *
     * @param CreateSlideItemRequest $request
     *
     * @return Response
     */
    public function store(CreateSlideItemRequest $request)
    {
        $input = $request->all();
        $image = "";
        if(isset($input['image'])){
            $request['file']=$input['image'];
            $input['image'] = upload($request);
        }
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $input['user_id'] = auth()->user()->id;

        $slideItem = $this->slideItemRepository->create($input);

        Flash::success('Slide Item saved successfully.');

        return redirect(route('slideItems.index'));
    }

    /**
     * Display the specified SlideItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $slideItem = $this->slideItemRepository->find($id);

        if (empty($slideItem)) {
            Flash::error('Slide Item not found');

            return redirect(route('slideItems.index'));
        }

        return view('slide_items.show')->with('slideItem', $slideItem);
    }

    /**
     * Show the form for editing the specified SlideItem.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $slideItem = $this->slideItemRepository->find($id);

        if (empty($slideItem)) {
            Flash::error('Slide Item not found');

            return redirect(route('slideItems.index'));
        }

        return view('slide_items.edit')->with('slideItem', $slideItem);
    }

    /**
     * Update the specified SlideItem in storage.
     *
     * @param int $id
     * @param UpdateSlideItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSlideItemRequest $request)
    {
        $slideItem = $this->slideItemRepository->find($id);

        if (empty($slideItem)) {
            Flash::error('Slide Item not found');

            return redirect(route('slideItems.index'));
        }

        $input = $request->all();
        $image = "";
        if(isset($input['image'])){
            $request['file']=$input['image'];
            $input['image'] = upload($request);
        }
        $input['status'] = ($input['status'] ==="activé") ? 1 : 0;
        $input['user_id'] = auth()->user()->id;

        $slideItem = $this->slideItemRepository->update($input, $id);

        Flash::success('Slide Item updated successfully.');

        return redirect(route('slideItems.index'));
    }

    /**
     * Remove the specified SlideItem from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $slideItem = $this->slideItemRepository->find($id);

        if (empty($slideItem)) {
            Flash::error('Slide Item not found');

            return redirect(route('slideItems.index'));
        }

        $this->slideItemRepository->delete($id);

        Flash::success('Slide Item deleted successfully.');

        return redirect(route('slideItems.index'));
    }
}
