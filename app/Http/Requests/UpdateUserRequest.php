<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = User::$rules;
        $rules['name'] = [
            'required', 'string',
            Rule::unique('users')->ignore($this->id),
        ];
        $rules['email'] = [
            'required', 'email',
            Rule::unique('users')->ignore($this->id),
        ];
        $rules['avatar'] = "nullable";
        $rules['password'] = "nullable|string";
        return $rules;
    }
}
