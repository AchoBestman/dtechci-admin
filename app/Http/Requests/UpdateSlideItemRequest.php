<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\SlideItem;
use Illuminate\Validation\Rule;

class UpdateSlideItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = SlideItem::$rules;
        $rules['libelle'] = [
            'required', 'string',
            Rule::unique('slide_items')->ignore($this->id),
        ];
        $rules['ordre'] = [
            'required', 'integer',
            Rule::unique('slide_items')->ignore($this->id),
        ];
        $rules['image'] = "nullable";
        return $rules;
    }
}
