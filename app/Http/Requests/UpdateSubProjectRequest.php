<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\SubProject;
use Illuminate\Validation\Rule;

class UpdateSubProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = SubProject::$rules;

        $rules['libelle'] = [
            'required', 'string',
            Rule::unique('sub_projects')->ignore($this->id),
        ];
        $rules['ordre'] = [
            'required', 'integer',
            Rule::unique('sub_projects')->ignore($this->id),
        ];
        $rules['image'] = "nullable";

        return $rules;
    }
}
