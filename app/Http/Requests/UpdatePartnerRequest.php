<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Partner;
use Illuminate\Validation\Rule;

class UpdatePartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Partner::$rules;

        $rules['libelle'] = [
            'required', 'string',
            Rule::unique('partners')->ignore($this->id),
        ];
        $rules['ordre'] = [
            'required', 'integer',
            Rule::unique('partners')->ignore($this->id),
        ];
        $rules['image'] = "nullable";

        return $rules;
    }
}
