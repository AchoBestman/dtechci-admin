<?php

namespace App\Http\Requests\API;

use App\Models\Slide;
use InfyOm\Generator\Request\APIRequest;

class UpdateSlideAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Slide::$rules;
        $rules['libelle'] = $rules['libelle'].",".$this->route("slide");$rules['type'] = $rules['type'].",".$this->route("slide");
        return $rules;
    }
}
