<?php

namespace App\Http\Requests\API;

use App\Models\SlideItem;
use InfyOm\Generator\Request\APIRequest;

class UpdateSlideItemAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = SlideItem::$rules;
        $rules['libelle'] = $rules['libelle'].",".$this->route("slide_item");$rules['ordre'] = $rules['ordre'].",".$this->route("slide_item");
        return $rules;
    }
}
