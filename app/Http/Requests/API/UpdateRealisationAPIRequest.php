<?php

namespace App\Http\Requests\API;

use App\Models\Realisation;
use InfyOm\Generator\Request\APIRequest;

class UpdateRealisationAPIRequest extends APIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Realisation::$rules;
        $rules['libelle'] = $rules['libelle'].",".$this->route("realisation");$rules['ordre'] = $rules['ordre'].",".$this->route("realisation");
        return $rules;
    }
}
