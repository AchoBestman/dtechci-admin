<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Realisation;
use Illuminate\Validation\Rule;

class UpdateRealisationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Realisation::$rules;
        $rules['libelle'] = [
            'required', 'string',
            Rule::unique('realisations')->ignore($this->id),
        ];
        $rules['ordre'] = [
            'required', 'integer',
            Rule::unique('realisations')->ignore($this->id),
        ];
        $rules['image'] = "nullable";
        return $rules;
    }
}
