<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Slide;
use Illuminate\Validation\Rule;

class UpdateSlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Slide::$rules;
        $rules['libelle'] = [
            'required', 'string',
            Rule::unique('slides')->ignore($this->id),
        ];
        $rules['type'] = [
            'required', 'string',
            Rule::unique('slides')->ignore($this->id),
        ];
        $rules['image'] = "nullable";
        return $rules;
    }
}
