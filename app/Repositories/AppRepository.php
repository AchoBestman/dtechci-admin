<?php

namespace App\Repositories;

use App\Models\App;
use App\Repositories\BaseRepository;

/**
 * Class AppRepository
 * @package App\Repositories
 * @version April 17, 2024, 7:39 am UTC
*/

class AppRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle',
        'ordre',
        'description',
        'status',
        'user_id',
        'map',
        'email',
        'address',
        'phone1',
        'phone2',
        'phone3',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return App::class;
    }
}
