<?php

namespace App\Repositories;

use App\Models\SlideItem;
use App\Repositories\BaseRepository;

/**
 * Class SlideItemRepository
 * @package App\Repositories
 * @version April 17, 2024, 7:40 am UTC
*/

class SlideItemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle',
        'ordre',
        'description',
        'status',
        'user_id',
        'btnText',
        'btnLink',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SlideItem::class;
    }
}
