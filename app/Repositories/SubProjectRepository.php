<?php

namespace App\Repositories;

use App\Models\SubProject;
use App\Repositories\BaseRepository;

/**
 * Class SubProjectRepository
 * @package App\Repositories
 * @version April 17, 2024, 7:41 am UTC
*/

class SubProjectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle',
        'description',
        'ordre',
        'status',
        'user_id',
        'project_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SubProject::class;
    }
}
