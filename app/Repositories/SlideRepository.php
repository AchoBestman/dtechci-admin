<?php

namespace App\Repositories;

use App\Models\Slide;
use App\Repositories\BaseRepository;

/**
 * Class SlideRepository
 * @package App\Repositories
 * @version April 17, 2024, 7:40 am UTC
*/

class SlideRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'libelle',
        'type',
        'user_id',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Slide::class;
    }
}
