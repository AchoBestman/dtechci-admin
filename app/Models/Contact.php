<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Contact
 * @package App\Models
 * @version April 18, 2024, 7:36 am UTC
 *
 * @property string $fullname
 * @property string $email
 * @property string $phone
 * @property string $subject
 * @property string $description
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 */
class Contact extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'contacts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'fullname',
        'email',
        'phone',
        'subject',
        'description',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fullname' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'subject' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'fullname' => 'required',
        'email' => 'nullable',
        'phone' => 'required',
        'subject' => 'required',
        'description' => 'required',
        'status' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}
