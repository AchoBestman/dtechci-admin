<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class User
 * @package App\Models
 * @version April 16, 2024, 12:20 pm UTC
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property boolean $status
 * @property string $avatar
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string role
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    use SoftDeletes;

    use HasFactory;

    public $table = 'users';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'email',
        'password',
        'status',
        'avatar',
        'role',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'password' => 'string',
        'status' => 'string',
        'avatar' => 'string',
        'role'=> 'string',
        'email_verified_at' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'role'=> 'nullable',
        'name' => 'required|unique:users',
        'email' => 'required|unique:users',
        'password' => 'required|min:6',
        'status' => 'required|string',
        'avatar' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
    
    public static function getStatus($item){
        return isset($item) ? $item['status'] == 1 ? 
            ["activé"=>"activé", "désactivé"=>"désactivé"] : ["désactivé"=>"désactivé", "activé"=>"activé"] : ["activé"=>"activé", "désactivé"=>"désactivé"];
    }
}
