<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class App
 * @package App\Models
 * @version April 17, 2024, 7:39 am UTC
 *
 * @property string $libelle
 * @property integer $ordre
 * @property string $description
 * @property boolean $status
 * @property integer $user_id
 * @property string $image
 * @property string $map
 * @property string $email
 * @property string $address
 * @property string $phone1
 * @property string $phone2
 * @property string $phone3
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class App extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'apps';
    
    protected $dates = ['deleted_at'];

    public $fillable = [
        'libelle',
        'ordre',
        'description',
        'status',
        'user_id',
        'image',
        'map',
        'email',
        'address',
        'phone1',
        'phone2',
        'phone3',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'libelle' => 'string',
        'ordre' => 'integer',
        'status' => 'boolean',
        'user_id' => 'integer',
        'image' => 'string',
        'map' => 'string',
        'email' => 'string',
        'address' => 'string',
        'phone1' => 'string',
        'phone2' => 'string',
        'phone3' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'libelle' => 'required|unique:apps',
        'ordre' => 'required|unique:apps',
        'description' => 'nullable',
        'status' => 'required',
        'user_id' => 'nullable',
        'image' => 'required',
        'map' => 'nullable',
        'email' => 'required',
        'address' => 'required',
        'phone1' => 'required',
        'phone2' => 'nullable',
        'phone3' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function user(){
        return  $this->belongsTo(\App\Models\User::class,'user_id');
    }
}
