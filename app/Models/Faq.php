<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Faq
 * @package App\Models
 * @version April 17, 2024, 11:48 am UTC
 *
 * @property string $question
 * @property string $response
 * @property integer $user_id
 * @property integer $ordre
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 */
class Faq extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'faqs';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'question',
        'response',
        'user_id',
        'ordre',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'question' => 'string',
        'user_id' => 'integer',
        'ordre' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'question' => 'required|unique:faqs',
        'response' => 'required',
        'user_id' => 'nullable',
        'ordre' => 'required',
        'status' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}
