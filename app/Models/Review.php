<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Review
 * @package App\Models
 * @version April 18, 2024, 7:47 am UTC
 *
 * @property string $fullname
 * @property string $job
 * @property string $description
 * @property integer $note
 * @property integer $sub_project_id
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 */
class Review extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'reviews';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'fullname',
        'job',
        'description',
        'note',
        'sub_project_id',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fullname' => 'string',
        'job' => 'string',
        'note' => 'integer',
        'sub_project_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'fullname' => 'required',
        'job' => 'nullable',
        'description' => 'nullable',
        'note' => 'required',
        'sub_project_id' => 'nullable',
        'status' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    public function subproject(){
        return  $this->belongsTo(\App\Models\SubProject::class,'sub_project_id');
    }
}