<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SlideItem
 * @package App\Models
 * @version April 17, 2024, 7:40 am UTC
 *
 * @property string $libelle
 * @property integer $ordre
 * @property string $description
 * @property boolean $status
 * @property integer $user_id
 * @property string $image
 * @property string $btnText
 * @property string $btnLink
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string slide_id
 */
class SlideItem extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'slide_items';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle',
        'ordre',
        'description',
        'status',
        'user_id',
        'slide_id',
        'image',
        'btnText',
        'btnLink',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'libelle' => 'string',
        'ordre' => 'integer',
        'status' => 'boolean',
        'user_id' => 'integer',
        'slide_id' => 'integer',
        'image' => 'string',
        'btnText' => 'string',
        'btnLink' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'libelle' => 'required|unique:slide_items',
        'ordre' => 'required|unique:slide_items',
        'description' => 'nullable',
        'status' => 'required',
        'user_id' => 'nullable',
        'slide_id' => 'nullable',
        'image' => 'required',
        'btnText' => 'nullable',
        'btnLink' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function user(){
        return  $this->belongsTo(\App\Models\User::class,'user_id');
    }

    public function slide(){
        return  $this->belongsTo(\App\Models\Slide::class,'slide_id');
    }
}
