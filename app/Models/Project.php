<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Project
 * @package App\Models
 * @version April 16, 2024, 12:21 pm UTC
 *
 * @property string $libelle
 * @property string $description
 * @property integer $ordre
 * @property boolean $status
 * @property integer $user_id
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Project extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'projects';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle',
        'description',
        'ordre',
        'status',
        'user_id',
        'image',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'libelle' => 'string',
        'ordre' => 'integer',
        'status' => 'boolean',
        'user_id' => 'integer',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'libelle' => 'required|unique:projects',
        'description' => 'required',
        'ordre' => 'required|unique:projects',
        'status' => 'required',
        'user_id' => 'nullable',
        'image' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function user(){
        return  $this->belongsTo(\App\Models\User::class,'user_id');
    }
}
