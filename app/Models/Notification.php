<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Notification
 * @package App\Models
 * @version April 18, 2024, 7:35 am UTC
 *
 * @property string $libelle
 * @property string $description
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 */
class Notification extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'notifications';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle',
        'description',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'libelle' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'libelle' => 'required',
        'description' => 'required',
        'status' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}
