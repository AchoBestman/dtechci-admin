<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Newsletter
 * @package App\Models
 * @version April 18, 2024, 7:34 am UTC
 *
 * @property string $email
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 */
class Newsletter extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'newsletters';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'email',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'email' => 'string',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'email' => 'required|unique:newsletters',
        'status' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}
