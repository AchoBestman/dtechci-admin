<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SubProject
 * @package App\Models
 * @version April 17, 2024, 7:41 am UTC
 *
 * @property string $libelle
 * @property string $description
 * @property integer $ordre
 * @property boolean $status
 * @property integer $user_id
 * @property integer $project_id
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class SubProject extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'sub_projects';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle',
        'description',
        'ordre',
        'status',
        'user_id',
        'project_id',
        'image',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'libelle' => 'string',
        'ordre' => 'integer',
        'status' => 'boolean',
        'user_id' => 'integer',
        'project_id' => 'integer',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'libelle' => 'required|unique:sub_projects',
        'description' => 'nullable',
        'ordre' => 'required',
        'status' => 'required',
        'user_id' => 'nullable',
        'project_id' => 'nullable',
        'image' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function user(){
        return  $this->belongsTo(\App\Models\User::class,'user_id');
    }

    public function project(){
        return  $this->belongsTo(\App\Models\Project::class,'project_id');
    }
}
