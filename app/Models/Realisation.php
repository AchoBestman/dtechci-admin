<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Realisation
 * @package App\Models
 * @version April 17, 2024, 7:40 am UTC
 *
 * @property string $libelle
 * @property integer $ordre
 * @property string $description
 * @property boolean $status
 * @property integer $user_id
 * @property string $image
 * @property string $link
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Realisation extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'realisations';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle',
        'ordre',
        'description',
        'status',
        'user_id',
        'image',
        'link',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'libelle' => 'string',
        'ordre' => 'integer',
        'status' => 'boolean',
        'user_id' => 'integer',
        'image' => 'string',
        'link' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'libelle' => 'required|unique:realisations',
        'ordre' => 'required|unique:realisations',
        'description' => 'nullable',
        'status' => 'required',
        'user_id' => 'nullable',
        'image' => 'required',
        'link' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function user(){
        return  $this->belongsTo(\App\Models\User::class,'user_id');
    }
}
