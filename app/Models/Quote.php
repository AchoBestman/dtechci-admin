<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Quote
 * @package App\Models
 * @version April 18, 2024, 7:39 am UTC
 *
 * @property string $fullname
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property string $category
 * @property string $subcategory
 * @property string $libelle
 * @property string $description
 * @property integer $quantity
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 */
class Quote extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'quotes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'fullname',
        'email',
        'phone',
        'address',
        'category',
        'subcategory',
        'libelle',
        'description',
        'quantity',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'fullname' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'address' => 'string',
        'category' => 'string',
        'subcategory' => 'string',
        'libelle' => 'string',
        'quantity' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'fullname' => 'required',
        'email' => 'nullable',
        'phone' => 'required',
        'address' => 'required',
        'category' => 'nullable',
        'subcategory' => 'nullable',
        'libelle' => 'required',
        'description' => 'nullable',
        'quantity' => 'nullable',
        'status' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}
