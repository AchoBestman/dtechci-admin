<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Slide
 * @package App\Models
 * @version April 17, 2024, 7:40 am UTC
 *
 * @property string $libelle
 * @property string $type
 * @property integer $user_id
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Slide extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'slides';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'libelle',
        'type',
        'user_id',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'libelle' => 'string',
        'type' => 'string',
        'user_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'nullable',
        'libelle' => 'required|unique:slides',
        'type' => 'required|unique:slides',
        'user_id' => 'nullable',
        'status' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function user(){
        return  $this->belongsTo(\App\Models\User::class,'user_id');
    }
}
