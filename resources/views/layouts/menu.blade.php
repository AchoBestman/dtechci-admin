
<li class="nav-item">
    <a href="{{ route('apps.index') }}"
       class="nav-link {{ Request::is('apps*') ? 'active' : '' }}">
        <p>Applications</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('users.index') }}"
       class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
        <p>Utilisateurs</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('projects.index') }}"
       class="nav-link {{ Request::is('projects*') ? 'active' : '' }}">
        <p>Projets</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('subProjects.index') }}"
       class="nav-link {{ Request::is('subProjects*') ? 'active' : '' }}">
        <p>Sous projets</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('realisations.index') }}"
       class="nav-link {{ Request::is('realisations*') ? 'active' : '' }}">
        <p>Réalisations</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('partners.index') }}"
       class="nav-link {{ Request::is('partners*') ? 'active' : '' }}">
        <p>Partenaires</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('contacts.index') }}"
       class="nav-link {{ Request::is('contacts*') ? 'active' : '' }}">
        <p>Contacts</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('quotes.index') }}"
       class="nav-link {{ Request::is('quotes*') ? 'active' : '' }}">
        <p>Demandes de dévis</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('faqs.index') }}"
       class="nav-link {{ Request::is('faqs*') ? 'active' : '' }}">
        <p>Foie aux questions</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('newsletters.index') }}"
       class="nav-link {{ Request::is('newsletters*') ? 'active' : '' }}">
        <p>Newsletters</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('notifications.index') }}"
       class="nav-link {{ Request::is('notifications*') ? 'active' : '' }}">
        <p>Notifications</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('reviews.index') }}"
       class="nav-link {{ Request::is('reviews*') ? 'active' : '' }}">
        <p>Avis utilisateurs</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('slides.index') }}"
       class="nav-link {{ Request::is('slides*') ? 'active' : '' }}">
        <p>Animations</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('slideItems.index') }}"
       class="nav-link {{ Request::is('slideItems*') ? 'active' : '' }}">
        <p>Contenus animations</p>
    </a>
</li>

