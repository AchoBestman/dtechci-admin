<!-- Fullname Field -->
<div class="col-sm-6">
    <p>{!! Form::label('fullname', 'Nom et Prenoms:') !!} {{ $review->fullname }}</p>
</div>

<!-- Job Field -->
<div class="col-sm-6">
    <p>{!! Form::label('job', 'Emploie:') !!} {{ $review->job }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-4">
    <p>{!! Form::label('sub_project_id', 'Projet: ') !!} {{ $review->subproject ? $review->subproject->libelle : $review->sub_project_id }}</p>
</div>

<!-- Note Field -->
<div class="col-sm-4">
    {!! Form::label('note', 'Note:') !!}
    <p>{{ $review->note }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-4">
    <p>
        {!! Form::label('status', 'Status: ') !!} 
        @if ($review->status == true)
        <span class="text-success">Traité</span>
        @else
        <span class="text-danger">Non traité</span>
        @endif
    </p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Commentaire:') !!}
    <p>{{ $review->description }}</p>
</div>


<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $review->created_at->format('d M. Y') }}</p>
</div>