<!-- Fullname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fullname', 'Nom et Prenoms:') !!}
    {!! Form::text('fullname', null, ['class' => 'form-control']) !!}
</div>

<!-- Job Field -->
<div class="form-group col-sm-6">
    {!! Form::label('job', 'Emploie:') !!}
    {!! Form::text('job', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sub_project_id', 'Projet') !!}
    {!! Form::select('sub_project_id', $projects, null, ['class' => 'form-control']) !!}
</div>

<!-- Note Field -->
<div class="form-group col-sm-3">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::select('note', [0, 1, 2, 3, 4, 5] null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-3">
    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
</div>

{!! Form::number('id', null, ['class' => 'form-control','hidden' => true]) !!}

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Commentaire:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>


