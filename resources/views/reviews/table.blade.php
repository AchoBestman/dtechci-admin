<div class="table-responsive">
    <table class="table" id="reviews-table">
        <thead>
        <tr>
            <th>Nom et Prenoms</th>
            <th>Note</th>
            <th>Date</th>
            <th>Etat</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reviews as $review)
            <tr>
                <td>{{ $review->fullname }}</td>
                <td>{{ $review->note }}</td>
                <td>{{ $review->created_at->format('d M. Y') }}</td>
                @if ($review->status == true)
                <td> <span class="text-success">Traité</span> </td>
                @else
                <td> <span class="text-danger">Non traité</span> </td> 
                @endif
                <td width="120">
                    {!! Form::open(['route' => ['reviews.destroy', $review->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('reviews.show', [$review->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('reviews.edit', [$review->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
