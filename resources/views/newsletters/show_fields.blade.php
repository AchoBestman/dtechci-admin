<!-- Email Field -->
<div class="col-sm-12">
    <p>{!! Form::label('email', 'Email:') !!} {{ $newsletter->email }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>{!! Form::label('status', 'Status: ') !!} {{ $newsletter->status == true ? "Activé" : "Désactivé"  }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $newsletter->created_at->format('d M. Y') }}</p>
</div>