<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('project_id', 'Projet: ') !!} {{ $subProject->project ? $subProject->project->libelle : $subProject->project_id }}</p>
</div>

<!-- Libelle Field -->
<div class="col-sm-12">
    <p>{!! Form::label('libelle', 'Libelle: ') !!} {{ $subProject->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    <p>{!! Form::label('description', 'Description: ') !!} {{ $subProject->description }}</p>
</div>

<!-- Ordre Field -->
<div class="col-sm-12">
    <p>{!! Form::label('ordre', 'Ordre: ') !!} {{ $subProject->ordre }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>{!! Form::label('status', 'Status: ') !!} {{ $subProject->status == true ? "Activé" : "Désactivé"  }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('user_id', 'Createur: ') !!} {{ $subProject->user ? $subProject->user->email : $subProject->user_id }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    <p>{!! Form::label('image', 'Image: ') !!}<img  src="{{ image_url($subProject->image) }}" alt="" class="img-fluid" style="width:200px; height: 200px"></p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $subProject->created_at->format('d M. Y') }}</p>
</div>
