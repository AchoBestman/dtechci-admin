<div class="table-responsive">
    <table class="table" id="subProjects-table">
        <thead>
        <tr>
        <th>Projet</th>
        <th>Libelle</th>
        <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subProjects as $subProject)
            <tr>
            <td>{{ $subProject->project? $subProject->project->libelle : $subProject->project_id }}</td>
            <td><img  src="{{ image_url($subProject->image) }}" alt="" class="user-image img-circle elevation-2" style="width:20px; height: 20px"> {{ $subProject->libelle }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['subProjects.destroy', $subProject->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('subProjects.show', [$subProject->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('subProjects.edit', [$subProject->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
