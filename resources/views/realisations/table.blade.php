<div class="table-responsive">
    <table class="table" id="realisations-table">
        <thead>
        <tr>
        <th>Libelle</th>
        <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($realisations as $realisation)
            <tr>
            <td><img  src="{{ image_url($realisation->image) }}" alt="" class="user-image img-circle elevation-2" style="width:20px; height: 20px">{{ $realisation->libelle }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['realisations.destroy', $realisation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('realisations.show', [$realisation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('realisations.edit', [$realisation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
