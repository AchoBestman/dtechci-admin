@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Modification de réalisation</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::model($realisation, ['route' => ['realisations.update', $realisation->id], 'method' => 'patch', 'files' => true]) !!}

            <div class="card-body">
                <div class="row">
                    @include('realisations.fields', ['status' => \App\Models\User::getStatus($realisation)])
                </div>
            </div>

            <div class="card-footer">
                {!! Form::submit('Modifier', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('realisations.index') }}" class="btn btn-default">Annuler</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
