<!-- Libelle Field -->
<div class="col-sm-12">
    <p>{!! Form::label('libelle', 'Libelle: ') !!} {{ $realisation->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    <p>{!! Form::label('description', 'Description: ') !!} {{ $realisation->description }}</p>
</div>

<!-- Ordre Field -->
<div class="col-sm-12">
    <p>{!! Form::label('ordre', 'Ordre: ') !!} {{ $realisation->ordre }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>{!! Form::label('status', 'Status: ') !!} {{ $realisation->status == true ? "Activé" : "Désactivé"  }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('user_id', 'Createur: ') !!} {{ $realisation->user ? $realisation->user->email : $realisation->user_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('link', 'Lien: ') !!} {{ $realisation->link  }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    <p>{!! Form::label('image', 'Image: ') !!}<img  src="{{ image_url($realisation->image) }}" alt="" class="img-fluid" style="width:200px; height: 200px"></p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $realisation->created_at->format('d M. Y') }}</p>
</div>

