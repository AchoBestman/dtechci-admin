<!-- Fullname Field -->
<div class="col-sm-6">
    <p>{!! Form::label('fullname', 'Fullname:') !!} {{ $quote->fullname }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-6">
    <p>{!! Form::label('email', 'Email:') !!} {{ $quote->email }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-6">
    <p>{!! Form::label('phone', 'Téléphone:') !!} {{ $quote->phone }}</p>
</div>

<!-- Address Field -->
<div class="col-sm-6">
    <p>{!! Form::label('address', 'Address:') !!} {{ $quote->address }}</p>
</div>

<!-- Category Field -->
<div class="col-sm-4">
    <p>{!! Form::label('category', 'Categorie:') !!} {{ $quote->category }}</p>
</div>

<!-- Subcategory Field -->
<div class="col-sm-4">
    <p>{!! Form::label('subcategory', 'Sous categorie:') !!} {{ $quote->subcategory }}</p>
</div>

<!-- Libelle Field -->
<div class="col-sm-4">
    <p>{!! Form::label('libelle', 'Article:') !!} {{ $quote->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description') !!}
    <p>{{ $quote->description }}</p>
</div>

<!-- Quantity Field -->
<div class="col-sm-4">
    <p>{!! Form::label('quantity', 'Quantity:') !!} {{ $quote->quantity }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-4">
    <p>
        {!! Form::label('status', 'Status: ') !!} 
        @if ($quote->status == true)
        <span class="text-success">Traité</span>
        @else
        <span class="text-danger">Non traité</span>
        @endif
    </p>
</div>

<!-- Created At Field -->
<div class="col-sm-4">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $quote->created_at->format('d M. Y') }}</p>
</div>

