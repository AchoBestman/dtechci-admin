<!-- Fullname Field -->
<div class="form-group col-sm-4">
    {!! Form::label('fullname', 'Client:') !!}
    {!! Form::text('fullname', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-4">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-4">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Libelle Field -->
<div class="form-group col-sm-12">
    {!! Form::label('libelle', 'Article:') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Field -->
<div class="form-group col-sm-4">
    {!! Form::label('category', 'Categorie:') !!}
    {!! Form::text('category', null, ['class' => 'form-control']) !!}
</div>

<!-- Subcategory Field -->
<div class="form-group col-sm-4">
    {!! Form::label('subcategory', 'Sous Categorie:') !!}
    {!! Form::text('subcategory', null, ['class' => 'form-control']) !!}
</div>


<!-- Status Field -->
<div class="form-group col-sm-4">
    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
</div>

{!! Form::number('id', null, ['class' => 'form-control','hidden' => true]) !!}

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

