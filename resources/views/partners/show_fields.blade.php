<!-- Libelle Field -->
<div class="col-sm-12">
    <p>{!! Form::label('libelle', 'Libelle: ') !!} {{ $partner->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    <p>{!! Form::label('description', 'Description: ') !!} {{ $partner->description }}</p>
</div>

<!-- Ordre Field -->
<div class="col-sm-12">
    <p>{!! Form::label('ordre', 'Ordre: ') !!} {{ $partner->ordre }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>{!! Form::label('status', 'Status: ') !!} {{ $partner->status == true ? "Activé" : "Désactivé"  }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('user_id', 'Createur: ') !!} {{ $partner->user ? $partner->user->email : $partner->user_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('link', 'Lien: ') !!} {{ $partner->link  }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    <p>{!! Form::label('image', 'Image: ') !!}<img  src="{{ image_url($partner->image) }}" alt="" class="img-fluid" style="width:200px; height: 200px"></p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $partner->created_at->format('d M. Y') }}</p>
</div>


