@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>Créer une application</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="card">

            {!! Form::open(['route' => 'apps.store', 'files' => true]) !!}

            <div class="card-body">

                <div class="row">
                    @include('apps.fields', ['status' => \App\Models\User::getStatus(null)])
                </div>

            </div>

            <div class="card-footer">
                {!! Form::submit('Enrégistrer', ['class' => 'btn btn-primary']) !!}
                <a href="{{ route('apps.index') }}" class="btn btn-default">Annuler</a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
