<!-- Libelle Field -->
<div class="form-group col-sm-5">
    {!! Form::label('libelle', 'Nom:') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
</div>
<!-- Ordre Field -->
<div class="form-group col-sm-2">
    {!! Form::label('ordre', 'Ordre:') !!}
    {!! Form::number('ordre', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-2">

    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
</div>

{!! Form::number('id', null, ['class' => 'form-control','hidden' => true]) !!}

<!-- Image Field -->
<div class="form-group col-sm-3">
    {!! Form::label('image', 'Logo:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('image', 'Choisir la photo', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>



<!-- Email Field -->
<div class="form-group col-sm-3">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-3">
    {!! Form::label('phone1', 'Téléphone 1:') !!}
    {!! Form::text('phone1', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-3">
    {!! Form::label('phone2', 'Téléphone 2:') !!}
    {!! Form::text('phone2', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-3">
    {!! Form::label('phone3', 'Téléphone 3:') !!}
    {!! Form::text('phone3', null, ['class' => 'form-control']) !!}
</div>

<!-- Map Field -->
<div class="form-group col-sm-6">
    {!! Form::label('map', 'Lien google map:') !!}
    {!! Form::text('map', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Adresse:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>
