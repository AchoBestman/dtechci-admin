<!-- Libelle Field -->
<div class="col-sm-12">
    <p>{!! Form::label('libelle', 'Nom: ') !!} {{ $app->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    <p>{!! Form::label('description', 'Description: ') !!} {{ $app->description }}</p>
</div>

<!-- Ordre Field -->
<div class="col-sm-12">
    <p>{!! Form::label('ordre', 'Ordre: ') !!} {{ $app->ordre }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>{!! Form::label('status', 'Status: ') !!} {{ $app->status == true ? "Activé" : "Désactivé"  }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('user_id', 'Createur: ') !!} {{ $app->user ? $app->user->email : $app->user_id }}</p>
</div>

<!-- Map Field -->
<div class="col-sm-12">
    <p>{!! Form::label('map', 'Lien map:') !!} {{ $app->map }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    <p>{!! Form::label('email', 'Email:') !!} {{ $app->email }}</p>
</div>

<!-- Address Field -->
<div class="col-sm-12">
    <p>{!! Form::label('address', 'Address:') !!} {{ $app->address }}</p>
</div>

<!-- Phone1 Field -->
<div class="col-sm-12">
    <p>{!! Form::label('phone1', 'Phone1:') !!} {{ $app->phone1 }}</p>
</div>

<!-- Phone2 Field -->
<div class="col-sm-12">
    <p>{!! Form::label('phone2', 'Phone2:') !!} {{ $app->phone2 }}</p>
</div>

<!-- Phone3 Field -->
<div class="col-sm-12">
    <p>{!! Form::label('phone3', 'Phone3:') !!} {{ $app->phone3 }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    <p>{!! Form::label('image', 'Logo: ') !!}<img  src="{{ image_url($app->image) }}" alt="" class="img-fluid" style="width:200px; height: 200px"></p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $app->created_at->format('d M. Y') }}</p>
</div>
