<div class="table-responsive">
    <table class="table" id="apps-table">
        <thead>
        <tr>
        <th>Nom</th>
        <th>email</th>
        <th>Téléphone</th>
        <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($apps as $app)
            <tr>
            <td><img  src="{{ image_url($app->image) }}" alt="" class="user-image img-circle elevation-2" style="width:20px; height: 20px"> {{ $app->libelle }}</td>
            <td>{{ $app->email }}</td>
            <td>{{ $app->phone1 }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['apps.destroy', $app->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('apps.show', [$app->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('apps.edit', [$app->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
