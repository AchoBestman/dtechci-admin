<!-- Question Field -->
<div class="col-sm-12">
    <p>{!! Form::label('question', 'Question:') !!} {{ $faq->question }}</p>
</div>

<!-- Response Field -->
<div class="col-sm-12">
    <p>{!! Form::label('response', 'Response:') !!} {{ $faq->response }}</p>
</div>

<!-- Ordre Field -->
<div class="col-sm-12">
    <p>{!! Form::label('ordre', 'Ordre: ') !!} {{ $faq->ordre }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>{!! Form::label('status', 'Status: ') !!} {{ $faq->status == true ? "Activé" : "Désactivé"  }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('user_id', 'Createur: ') !!} {{ $faq->user ? $faq->user->email : $faq->user_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $faq->created_at->format('d M. Y') }}</p>
</div>

