<!-- Question Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<!-- Response Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('response', 'Reponse:') !!}
    {!! Form::textarea('response', null, ['class' => 'form-control']) !!}
</div>

<!-- Ordre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ordre', 'Ordre:') !!}
    {!! Form::number('ordre', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-3">
    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
</div>
