<!-- Name Field -->
<div class="col-sm-12">
    <p>{!! Form::label('name', 'Nom et Prenoms:') !!} {{ $user->name }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    
    <p>{!! Form::label('email', 'Email:') !!} {{ $user->email }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>{!! Form::label('status', 'Statut:') !!} {{ $user->status == true ? "Activé" : "Désactivé" }}</p>
</div>

<!-- Avatar Field -->
<div class="col-sm-12">
    <p>{!! Form::label('avatar', 'Avatar: ') !!}<img  src="{{ image_url($user->avatar) }}" alt="" class="img-fluid" style="width:200px; height: 200px"></p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date:') !!} {{ $user->created_at->format('d M. Y, H:m:s') }}</p>
</div>

