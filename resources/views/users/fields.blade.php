<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Nom:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-5">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::text('password', null, ['class' => 'form-control','minlength' => 6]) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-2">

    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
</div>

{!! Form::number('id', null, ['class' => 'form-control','hidden' => true]) !!}

<!-- Avatar Field -->
<div class="form-group col-sm-5">
    {!! Form::label('avatar', 'Avatar:') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('avatar', ['class' => 'custom-file-input']) !!}
            {!! Form::label('avatar', 'Choose file', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>
<div class="clearfix"></div>
