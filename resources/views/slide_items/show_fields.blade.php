<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('slide_id', 'Animation: ') !!} {{ $slideItem->slide ? $slideItem->slide->libelle : $slideItem->slide_id }}</p>
</div>

<!-- Libelle Field -->
<div class="col-sm-12">
    <p>{!! Form::label('libelle', 'Libelle: ') !!} {{ $slideItem->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    <p>{!! Form::label('description', 'Description: ') !!} {{ $slideItem->description }}</p>
</div>

<!-- Ordre Field -->
<div class="col-sm-12">
    <p>{!! Form::label('ordre', 'Ordre: ') !!} {{ $slideItem->ordre }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>{!! Form::label('status', 'Status: ') !!} {{ $slideItem->status == true ? "Activé" : "Désactivé"  }}</p>
</div>


<!-- Btntext Field -->
<div class="col-sm-12">
    <p>    {!! Form::label('btnText', 'Texte boutton:') !!} {{ $slideItem->btnText }}</p>
</div>

<!-- Btnlink Field -->
<div class="col-sm-12">
    <p>{!! Form::label('btnLink', 'Lien boutton:') !!} {{ $slideItem->btnLink }}</p>
</div>


<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('user_id', 'Createur: ') !!} {{ $slideItem->user ? $slideItem->user->email : $slideItem->user_id }}</p>
</div>

<!-- Image Field -->
<div class="col-sm-12">
    <p>{!! Form::label('image', 'Image: ') !!}<img  src="{{ image_url($slideItem->image) }}" alt="" class="img-fluid" style="width:200px; height: 200px"></p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $slideItem->created_at->format('d M. Y') }}</p>
</div>


