<div class="table-responsive">
    <table class="table" id="slideItems-table">
        <thead>
        <tr>
        <th>Titre</th>
        <th>Animation</th>
        <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($slideItems as $slideItem)
            <tr>
            <td><img  src="{{ image_url($slideItem->image) }}" alt="" class="user-image img-circle elevation-2" style="width:20px; height: 20px"> {{ $slideItem->libelle }}</td>
            <td>{{ $slideItem->slide? $slideItem->slide->libelle : '' }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['slideItems.destroy', $slideItem->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('slideItems.show', [$slideItem->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('slideItems.edit', [$slideItem->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
