<!-- Status Field -->
<div class="form-group col-sm-2">
    {!! Form::label('slide_id', 'Animation') !!}
    {!! Form::select('slide_id', $slides, null, ['class' => 'form-control']) !!}
</div>

<!-- Libelle Field -->
<div class="form-group col-sm-10">
    {!! Form::label('libelle', 'Titre') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
</div>
<!-- Ordre Field -->
<div class="form-group col-sm-2">
    {!! Form::label('ordre', 'Ordre') !!}
    {!! Form::number('ordre', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-2">

    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
</div>

{!! Form::number('id', null, ['class' => 'form-control','hidden' => true]) !!}

<!-- Image Field -->
<div class="form-group col-sm-3">
    {!! Form::label('image', 'Image') !!}
    <div class="input-group">
        <div class="custom-file">
            {!! Form::file('image', ['class' => 'custom-file-input']) !!}
            {!! Form::label('image', 'Choisir la photo', ['class' => 'custom-file-label']) !!}
        </div>
    </div>
</div>

<!-- Btntext Field -->
<div class="form-group col-sm-2">
    {!! Form::label('btnText', 'Texte boutton') !!}
    {!! Form::text('btnText', null, ['class' => 'form-control']) !!}
</div>

<!-- Btnlink Field -->
<div class="form-group col-sm-3">
    {!! Form::label('btnLink', 'Lien boutton') !!}
    {!! Form::text('btnLink', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>
