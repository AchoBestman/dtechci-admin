<!-- Fullname Field -->
<div class="col-sm-12">
    <p>{!! Form::label('fullname', 'Nom et Prenoms:') !!} {{ $contact->fullname }}</p>
</div>

<!-- Email Field -->
<div class="col-sm-12">
    <p>{!! Form::label('email', 'Email:') !!} {{ $contact->email }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    <p>{!! Form::label('phone', 'Phone:') !!} {{ $contact->phone }}</p>
</div>

<!-- Subject Field -->
<div class="col-sm-12">
    <p>{!! Form::label('subject', 'Sujet:') !!} {{ $contact->subject }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Message') !!}
    <p>{{ $contact->description }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>
        {!! Form::label('status', 'Status: ') !!} 
        @if ($contact->status == true)
        <span class="text-success">Traité</span>
        @else
        <span class="text-danger">Non traité</span>
        @endif
    </p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $contact->created_at->format('d M. Y') }}</p>
</div>

