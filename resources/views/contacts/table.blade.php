<div class="table-responsive">
    <table class="table" id="contacts-table">
        <thead>
        <tr>
            <th>Nom et Prenoms</th>
            <th>Téléphone</th>
            <th>Sujet</th>
            <th>Date</th>
            <th>Etat</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($contacts as $contact)
            <tr>
                <td>{{ $contact->fullname }}</td>
                <td>{{ $contact->phone }}</td>
                <td>{{ $contact->subject }}</td>
                <td>{{ $contact->created_at->format('d M. Y') }}</td>
                @if ($contact->status == true)
                <td> <span class="text-success">Traité</span> </td>
                @else
                <td> <span class="text-danger">Non traité</span> </td> 
                @endif
                <td width="120">
                    {!! Form::open(['route' => ['contacts.destroy', $contact->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('contacts.show', [$contact->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('contacts.edit', [$contact->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
