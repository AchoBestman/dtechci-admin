<!-- Libelle Field -->
<div class="col-sm-12">
    <p>{!! Form::label('libelle', 'Libelle:') !!} {{ $slide->libelle }}</p>
</div>

<!-- Type Field -->
<div class="col-sm-12">
    <p>{!! Form::label('type', 'Type:') !!} {{ $slide->type }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    <p>{!! Form::label('status', 'Status: ') !!} {{ $slide->status == true ? "Activé" : "Désactivé"  }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    <p>{!! Form::label('user_id', 'Createur: ') !!} {{ $slide->user ? $slide->user->email : $slide->user_id }}</p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $slide->created_at->format('d M. Y') }}</p>
</div>

