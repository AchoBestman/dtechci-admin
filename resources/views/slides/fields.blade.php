<!-- Libelle Field -->
<div class="form-group col-sm-8">
    {!! Form::label('libelle', 'Libelle:') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-2">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-2">
    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
</div>

{!! Form::number('id', null, ['class' => 'form-control','hidden' => true]) !!}
