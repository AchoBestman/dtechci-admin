<!-- Libelle Field -->
<div class="col-sm-12">
    <p>{!! Form::label('libelle', 'Titre') !!} {{ $notification->libelle }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Contenu') !!}
    <p>{{ $notification->description }}</p>
</div>


<!-- Status Field -->
<div class="col-sm-12">
    <p>
        {!! Form::label('status', 'Status: ') !!} 
        @if ($notification->status == true)
        <span class="text-success">Envoyé</span>
        @else
        <span class="text-danger">Non envoyé</span>
        @endif
    </p>
</div>

<!-- Created At Field -->
<div class="col-sm-12">
    <p>{!! Form::label('created_at', 'Date: ') !!} {{ $notification->created_at->format('d M. Y') }}</p>
</div>