<!-- Libelle Field -->
<div class="form-group col-sm-9">
    {!! Form::label('libelle', 'Titre:') !!}
    {!! Form::text('libelle', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-3">
    {!! Form::label('status', 'Status') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
</div>

{!! Form::number('id', null, ['class' => 'form-control','hidden' => true]) !!}

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Contenu:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>


