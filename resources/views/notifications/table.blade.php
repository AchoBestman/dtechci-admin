<div class="table-responsive">
    <table class="table" id="notifications-table">
        <thead>
        <tr>
            <th>Titre</th>
            <th>Date</th>
            <th>Etat</th>
            <th colspan="3">Action</th>
            
        </tr>
        </thead>
        <tbody>
        @foreach($notifications as $notification)
            <tr>
                <td>{{ $notification->libelle }}</td>
                <td>{{ $notification->created_at->format('d M. Y') }}</td>
                @if ($notification->status == true)
                <td> <span class="text-success">Envoyé</span> </td>
                @else
                <td> <span class="text-danger">Non envoyé</span> </td> 
                @endif
                <td width="120">
                    {!! Form::open(['route' => ['notifications.destroy', $notification->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('notifications.show', [$notification->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('notifications.edit', [$notification->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
