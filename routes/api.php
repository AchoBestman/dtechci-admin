<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('projects', App\Http\Controllers\API\ProjectAPIController::class)->only('index', 'show');
Route::resource('apps', App\Http\Controllers\API\AppAPIController::class)->only('index', 'show');
Route::resource('partners', App\Http\Controllers\API\PartnerAPIController::class)->only('index', 'show');
Route::resource('realisations', App\Http\Controllers\API\RealisationAPIController::class)->only('index', 'show');
Route::resource('slides', App\Http\Controllers\API\SlideAPIController::class)->only('index', 'show');
Route::resource('slide_items', App\Http\Controllers\API\SlideItemAPIController::class)->only('index', 'show');
Route::resource('sub_projects', App\Http\Controllers\API\SubProjectAPIController::class)->only('index', 'show');
Route::resource('faqs', App\Http\Controllers\API\FaqAPIController::class)->only('index', 'show');
Route::resource('newsletters', App\Http\Controllers\API\NewsletterAPIController::class)->only('store','index');
Route::resource('contacts', App\Http\Controllers\API\ContactAPIController::class)->only('store','index');
Route::resource('quotes', App\Http\Controllers\API\QuoteAPIController::class)->only('store','index');
Route::resource('reviews', App\Http\Controllers\API\ReviewAPIController::class)->only('store','index');
