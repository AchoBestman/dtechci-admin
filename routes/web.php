<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');

Route::middleware('auth')->group(function () {

    Route::get('/', function () {
        return redirect('/apps');
    })->name('welcome');

    Route::get('/home', function () {
        return redirect('/apps');
    })->name('home');

    //Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('projects', App\Http\Controllers\ProjectController::class);
    Route::resource('users', App\Http\Controllers\UserController::class);
    Route::resource('subProjects', App\Http\Controllers\SubProjectController::class);
    Route::resource('slides', App\Http\Controllers\SlideController::class);
    Route::resource('slideItems', App\Http\Controllers\SlideItemController::class);
    Route::resource('realisations', App\Http\Controllers\RealisationController::class);
    Route::resource('partners', App\Http\Controllers\PartnerController::class);
    Route::resource('apps', App\Http\Controllers\AppController::class);
    Route::resource('slideItems', App\Http\Controllers\SlideItemController::class);
    Route::resource('subProjects', App\Http\Controllers\SubProjectController::class);
    Route::resource('faqs', App\Http\Controllers\FaqController::class);
    Route::resource('newsletters', App\Http\Controllers\NewsletterController::class);
    Route::resource('notifications', App\Http\Controllers\NotificationController::class);
    Route::resource('contacts', App\Http\Controllers\ContactController::class);
    Route::resource('quotes', App\Http\Controllers\QuoteController::class);
    Route::resource('reviews', App\Http\Controllers\ReviewController::class);
});

