<?php namespace Tests\Repositories;

use App\Models\Newsletter;
use App\Repositories\NewsletterRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class NewsletterRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var NewsletterRepository
     */
    protected $newsletterRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->newsletterRepo = \App::make(NewsletterRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_newsletter()
    {
        $newsletter = Newsletter::factory()->make()->toArray();

        $createdNewsletter = $this->newsletterRepo->create($newsletter);

        $createdNewsletter = $createdNewsletter->toArray();
        $this->assertArrayHasKey('id', $createdNewsletter);
        $this->assertNotNull($createdNewsletter['id'], 'Created Newsletter must have id specified');
        $this->assertNotNull(Newsletter::find($createdNewsletter['id']), 'Newsletter with given id must be in DB');
        $this->assertModelData($newsletter, $createdNewsletter);
    }

    /**
     * @test read
     */
    public function test_read_newsletter()
    {
        $newsletter = Newsletter::factory()->create();

        $dbNewsletter = $this->newsletterRepo->find($newsletter->id);

        $dbNewsletter = $dbNewsletter->toArray();
        $this->assertModelData($newsletter->toArray(), $dbNewsletter);
    }

    /**
     * @test update
     */
    public function test_update_newsletter()
    {
        $newsletter = Newsletter::factory()->create();
        $fakeNewsletter = Newsletter::factory()->make()->toArray();

        $updatedNewsletter = $this->newsletterRepo->update($fakeNewsletter, $newsletter->id);

        $this->assertModelData($fakeNewsletter, $updatedNewsletter->toArray());
        $dbNewsletter = $this->newsletterRepo->find($newsletter->id);
        $this->assertModelData($fakeNewsletter, $dbNewsletter->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_newsletter()
    {
        $newsletter = Newsletter::factory()->create();

        $resp = $this->newsletterRepo->delete($newsletter->id);

        $this->assertTrue($resp);
        $this->assertNull(Newsletter::find($newsletter->id), 'Newsletter should not exist in DB');
    }
}
