<?php namespace Tests\Repositories;

use App\Models\SlideItem;
use App\Repositories\SlideItemRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SlideItemRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SlideItemRepository
     */
    protected $slideItemRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->slideItemRepo = \App::make(SlideItemRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_slide_item()
    {
        $slideItem = SlideItem::factory()->make()->toArray();

        $createdSlideItem = $this->slideItemRepo->create($slideItem);

        $createdSlideItem = $createdSlideItem->toArray();
        $this->assertArrayHasKey('id', $createdSlideItem);
        $this->assertNotNull($createdSlideItem['id'], 'Created SlideItem must have id specified');
        $this->assertNotNull(SlideItem::find($createdSlideItem['id']), 'SlideItem with given id must be in DB');
        $this->assertModelData($slideItem, $createdSlideItem);
    }

    /**
     * @test read
     */
    public function test_read_slide_item()
    {
        $slideItem = SlideItem::factory()->create();

        $dbSlideItem = $this->slideItemRepo->find($slideItem->id);

        $dbSlideItem = $dbSlideItem->toArray();
        $this->assertModelData($slideItem->toArray(), $dbSlideItem);
    }

    /**
     * @test update
     */
    public function test_update_slide_item()
    {
        $slideItem = SlideItem::factory()->create();
        $fakeSlideItem = SlideItem::factory()->make()->toArray();

        $updatedSlideItem = $this->slideItemRepo->update($fakeSlideItem, $slideItem->id);

        $this->assertModelData($fakeSlideItem, $updatedSlideItem->toArray());
        $dbSlideItem = $this->slideItemRepo->find($slideItem->id);
        $this->assertModelData($fakeSlideItem, $dbSlideItem->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_slide_item()
    {
        $slideItem = SlideItem::factory()->create();

        $resp = $this->slideItemRepo->delete($slideItem->id);

        $this->assertTrue($resp);
        $this->assertNull(SlideItem::find($slideItem->id), 'SlideItem should not exist in DB');
    }
}
