<?php namespace Tests\Repositories;

use App\Models\Realisation;
use App\Repositories\RealisationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class RealisationRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var RealisationRepository
     */
    protected $realisationRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->realisationRepo = \App::make(RealisationRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_realisation()
    {
        $realisation = Realisation::factory()->make()->toArray();

        $createdRealisation = $this->realisationRepo->create($realisation);

        $createdRealisation = $createdRealisation->toArray();
        $this->assertArrayHasKey('id', $createdRealisation);
        $this->assertNotNull($createdRealisation['id'], 'Created Realisation must have id specified');
        $this->assertNotNull(Realisation::find($createdRealisation['id']), 'Realisation with given id must be in DB');
        $this->assertModelData($realisation, $createdRealisation);
    }

    /**
     * @test read
     */
    public function test_read_realisation()
    {
        $realisation = Realisation::factory()->create();

        $dbRealisation = $this->realisationRepo->find($realisation->id);

        $dbRealisation = $dbRealisation->toArray();
        $this->assertModelData($realisation->toArray(), $dbRealisation);
    }

    /**
     * @test update
     */
    public function test_update_realisation()
    {
        $realisation = Realisation::factory()->create();
        $fakeRealisation = Realisation::factory()->make()->toArray();

        $updatedRealisation = $this->realisationRepo->update($fakeRealisation, $realisation->id);

        $this->assertModelData($fakeRealisation, $updatedRealisation->toArray());
        $dbRealisation = $this->realisationRepo->find($realisation->id);
        $this->assertModelData($fakeRealisation, $dbRealisation->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_realisation()
    {
        $realisation = Realisation::factory()->create();

        $resp = $this->realisationRepo->delete($realisation->id);

        $this->assertTrue($resp);
        $this->assertNull(Realisation::find($realisation->id), 'Realisation should not exist in DB');
    }
}
