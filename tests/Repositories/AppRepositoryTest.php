<?php namespace Tests\Repositories;

use App\Models\App;
use App\Repositories\AppRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AppRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AppRepository
     */
    protected $appRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->appRepo = \App::make(AppRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_app()
    {
        $app = App::factory()->make()->toArray();

        $createdApp = $this->appRepo->create($app);

        $createdApp = $createdApp->toArray();
        $this->assertArrayHasKey('id', $createdApp);
        $this->assertNotNull($createdApp['id'], 'Created App must have id specified');
        $this->assertNotNull(App::find($createdApp['id']), 'App with given id must be in DB');
        $this->assertModelData($app, $createdApp);
    }

    /**
     * @test read
     */
    public function test_read_app()
    {
        $app = App::factory()->create();

        $dbApp = $this->appRepo->find($app->id);

        $dbApp = $dbApp->toArray();
        $this->assertModelData($app->toArray(), $dbApp);
    }

    /**
     * @test update
     */
    public function test_update_app()
    {
        $app = App::factory()->create();
        $fakeApp = App::factory()->make()->toArray();

        $updatedApp = $this->appRepo->update($fakeApp, $app->id);

        $this->assertModelData($fakeApp, $updatedApp->toArray());
        $dbApp = $this->appRepo->find($app->id);
        $this->assertModelData($fakeApp, $dbApp->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_app()
    {
        $app = App::factory()->create();

        $resp = $this->appRepo->delete($app->id);

        $this->assertTrue($resp);
        $this->assertNull(App::find($app->id), 'App should not exist in DB');
    }
}
