<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\App;

class AppApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_app()
    {
        $app = App::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/apps', $app
        );

        $this->assertApiResponse($app);
    }

    /**
     * @test
     */
    public function test_read_app()
    {
        $app = App::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/apps/'.$app->id
        );

        $this->assertApiResponse($app->toArray());
    }

    /**
     * @test
     */
    public function test_update_app()
    {
        $app = App::factory()->create();
        $editedApp = App::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/apps/'.$app->id,
            $editedApp
        );

        $this->assertApiResponse($editedApp);
    }

    /**
     * @test
     */
    public function test_delete_app()
    {
        $app = App::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/apps/'.$app->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/apps/'.$app->id
        );

        $this->response->assertStatus(404);
    }
}
