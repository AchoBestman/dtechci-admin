<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Quote;

class QuoteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_quote()
    {
        $quote = Quote::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/quotes', $quote
        );

        $this->assertApiResponse($quote);
    }

    /**
     * @test
     */
    public function test_read_quote()
    {
        $quote = Quote::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/quotes/'.$quote->id
        );

        $this->assertApiResponse($quote->toArray());
    }

    /**
     * @test
     */
    public function test_update_quote()
    {
        $quote = Quote::factory()->create();
        $editedQuote = Quote::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/quotes/'.$quote->id,
            $editedQuote
        );

        $this->assertApiResponse($editedQuote);
    }

    /**
     * @test
     */
    public function test_delete_quote()
    {
        $quote = Quote::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/quotes/'.$quote->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/quotes/'.$quote->id
        );

        $this->response->assertStatus(404);
    }
}
