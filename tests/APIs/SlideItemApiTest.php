<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SlideItem;

class SlideItemApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_slide_item()
    {
        $slideItem = SlideItem::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/slide_items', $slideItem
        );

        $this->assertApiResponse($slideItem);
    }

    /**
     * @test
     */
    public function test_read_slide_item()
    {
        $slideItem = SlideItem::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/slide_items/'.$slideItem->id
        );

        $this->assertApiResponse($slideItem->toArray());
    }

    /**
     * @test
     */
    public function test_update_slide_item()
    {
        $slideItem = SlideItem::factory()->create();
        $editedSlideItem = SlideItem::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/slide_items/'.$slideItem->id,
            $editedSlideItem
        );

        $this->assertApiResponse($editedSlideItem);
    }

    /**
     * @test
     */
    public function test_delete_slide_item()
    {
        $slideItem = SlideItem::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/slide_items/'.$slideItem->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/slide_items/'.$slideItem->id
        );

        $this->response->assertStatus(404);
    }
}
