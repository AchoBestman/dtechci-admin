<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Realisation;

class RealisationApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_realisation()
    {
        $realisation = Realisation::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/realisations', $realisation
        );

        $this->assertApiResponse($realisation);
    }

    /**
     * @test
     */
    public function test_read_realisation()
    {
        $realisation = Realisation::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/realisations/'.$realisation->id
        );

        $this->assertApiResponse($realisation->toArray());
    }

    /**
     * @test
     */
    public function test_update_realisation()
    {
        $realisation = Realisation::factory()->create();
        $editedRealisation = Realisation::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/realisations/'.$realisation->id,
            $editedRealisation
        );

        $this->assertApiResponse($editedRealisation);
    }

    /**
     * @test
     */
    public function test_delete_realisation()
    {
        $realisation = Realisation::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/realisations/'.$realisation->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/realisations/'.$realisation->id
        );

        $this->response->assertStatus(404);
    }
}
