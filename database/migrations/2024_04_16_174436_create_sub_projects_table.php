<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubProjectsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('libelle')->unique();
            $table->longText('description')->nullable(true);
            $table->integer('ordre');
            $table->boolean('status')->default(true);
            $table->integer('user_id')->nullable(true);
            $table->integer('project_id')->nullable(true);
            $table->string('image')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_projects');
    }
}
