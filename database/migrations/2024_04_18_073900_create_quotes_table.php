<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->string('email')->nullable(true);
            $table->string('phone');
            $table->string('address');
            $table->string('category')->nullable(true);
            $table->string('subcategory')->nullable(true);
            $table->string('libelle');
            $table->longText('description');
            $table->integer('quantity')->nullable(true);
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quotes');
    }
}
