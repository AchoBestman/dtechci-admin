<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('libelle')->unique();
            $table->integer('ordre');
            $table->longText('description')->nullable(true);
            $table->boolean('status')->default(true);
            $table->integer('user_id')->nullable(true);
            $table->string('image')->nullable(true);
            $table->string('map')->nullable(true);
            $table->string('email')->nullable(true);
            $table->string('address')->nullable(true);
            $table->string('phone1')->nullable(true);
            $table->string('phone2')->nullable(true);
            $table->string('phone3')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apps');
    }
}
