<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideItemsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slide_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('libelle')->unique();
            $table->integer('ordre');
            $table->longText('description')->nullable(true);
            $table->boolean('status')->default(true);
            $table->integer('user_id')->nullable(true);
            $table->integer('slide_id')->nullable(true);
            $table->string('image')->nullable(true);
            $table->string('btnText')->nullable(true);
            $table->string('btnLink')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('slide_items');
    }
}
