<?php

namespace Database\Factories;

use App\Models\SlideItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class SlideItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SlideItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->word,
        'ordre' => $this->faker->randomDigitNotNull,
        'description' => $this->faker->word,
        'status' => $this->faker->word,
        'user_id' => $this->faker->randomDigitNotNull,
        'image' => $this->faker->word,
        'btnText' => $this->faker->word,
        'btnLink' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
