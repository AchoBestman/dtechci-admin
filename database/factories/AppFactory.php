<?php

namespace Database\Factories;

use App\Models\App;
use Illuminate\Database\Eloquent\Factories\Factory;

class AppFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = App::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->word,
        'ordre' => $this->faker->randomDigitNotNull,
        'description' => $this->faker->word,
        'status' => $this->faker->word,
        'user_id' => $this->faker->randomDigitNotNull,
        'image' => $this->faker->word,
        'map' => $this->faker->word,
        'email' => $this->faker->word,
        'address' => $this->faker->word,
        'phone1' => $this->faker->word,
        'phone2' => $this->faker->word,
        'phone3' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
