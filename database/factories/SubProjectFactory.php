<?php

namespace Database\Factories;

use App\Models\SubProject;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubProject::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->word,
        'description' => $this->faker->word,
        'ordre' => $this->faker->randomDigitNotNull,
        'status' => $this->faker->word,
        'user_id' => $this->faker->randomDigitNotNull,
        'project_id' => $this->faker->randomDigitNotNull,
        'image' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
