<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            "name" => "Root user",
            "avatar" => "",
            "email" => "root@gmail.com",
            "created_at" => new \DateTime("now"),
            "role" => "root",
            "password" => Hash::make("root@gmail.com"),
            "status" => 1,
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
